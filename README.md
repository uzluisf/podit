NAME
====



`podit` - Convert a Pod document to PDF.

SYNOPSY
=======



    =begin pod
    =TITLE A Sample Document
    =AUTHOR Author 1 | Author 2
    =DATE June 6, 2006

    This is a sample Pod document.
    =end pod

DESCRIPTION
===========



`podit` is a module with the simple task of converting Pod documents to PDF through [Pandoc](https://www.pandoc.org).

The script arose from an interest of using Pod for writing regular documents and converting to PDFs with some metadata. The script will only look for the following named blocks in the Pod document whose contents are treated as [metadata](https://pandoc.org/MANUAL.html#metadata-variables):

    TITLE SUBTITLE AUTHOR DATE ABSTRACT KEYWORDS SUBJECT DESCRIPTION CATEGORY

For metadata variables (i.e., `AUTHOR`, `KEYWORDS`, etc.) that can accept several entries, use `|` as an entry separator.

AUTHOR
======

Luis F. Uceta

DATE
====

May 01, 2019

